#!/bin/bash

# Checking the current date on the system

echo "Current Date" >> /tmp/serverinfo.info
date  >> /tmp/serverinfo.info
echo " "  >> /tmp/serverinfo.info

# Getting the last 10 users that logged into the server

echo "Last 10 users that logged into the server" >> /tmp/serverinfo.info
last -n 10 |head >> /tmp/serverinfo.info
echo " "  >> /tmp/serverinfo.info

# Checking the swap space on the sever

echo "Swap space" >> /tmp/serverinfo.info
free -m >> /tmp/serverinfo.info
echo " "  >> /tmp/serverinfo.info

# Checking the kernel version of the server

echo "Kernel version" >> /tmp/serverinfo.info
uname -r >> /tmp/serverinfo.info
echo " "  >> /tmp/serverinfo.info

# Checking the Ip address on the server

echo "IP Address" >> /tmp/serverinfo.info
hostname -I >> /tmp/serverinfo.info





